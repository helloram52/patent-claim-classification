# README #

### What is this repository for? ###

* This repository is meant to track our work on categorizing US patent claims filed to date using machine learning/text processing algorithms.
* It involves two phases.
    * Splitting Phase
        * Patent claims filed before year 2000 are in raw text files and these need to converted to a format that the categorizing phase can handle.
    * Categorizing Phase
        * Patent claims are categorized into one of product, process or their hybrid i.e. product by process claims.

### How do I get set up? ###

* Download patent claims from [here](http://www.google.com/googlebooks/uspto-patents-grants-text.html). They've folders named after the year they were filed for and in them `*wk_XX.zip` files pertaining to patent claims that were filed in that week.

* By default, the script processes all the ZIP files in the current working directory (OS independent) and writes the output.csv and exception.csv to the same location. To alter the behavior, use one of the options below.

* To process all ZIP files in a directory, try the below.
> python parse_text_patent_claims.py -d DIR_WITH_ZIPPED_PATENT_FILES
* To process only a specific zip file(s), try the below.
> python parse_text_patent_claims.py -z ZIP_FILE1,ZIP_FILE2,ZIP_FILE3
* To process only for a specific extracted txt file(s), try the below.
> python parse_text_patent_claims.py -f EXTRACTED_ZIP_FILE1,FILE2,FILE3
* To write the output.csv and exception.csv to a different location, try the below.
> python parse_text_patent_claims.py -d DIR_WITH_ZIPPED_PATENT_FILES -o OUTPUT_DIRECTORY

### Who do I talk to? ###

* Vadivel @ gct.vadivel@gmail.com
* Ramkumar @ helloram52@gmail.com