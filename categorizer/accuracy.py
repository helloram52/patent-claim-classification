import csv,copy
import sys

#our output
pred = open(sys.argv[1])

#amt result
amt = open(sys.argv[2])
#output a csv file for those wrongly categorized
ex=open(sys.argv[3],"w")

amt = csv.reader(amt)
pred = csv.reader(pred)

#list stores all amt lines
amtlist = []
#list stores all prediction lines
predlist = []
#list stores all wrongly categorized
exlist = []

for each in amt:
    amtlist.append(each)

for each in pred:
    if each[6]=="1":
        predlist.append(each)
predlist = predlist[1:]
amtlist = amtlist[1:]

count=0
total = len(amtlist)

for a in amtlist:
    pnumber = a[0]
    cnumber = a[1]
    category = a[2]
    for p in predlist:
        if p[1]==pnumber and p[3]==cnumber and p[6]=="1":
            try:
                if p[7]==category:
                    count+=1
                else:
                    exception = [pnumber,cnumber,category,p[7]]
                    exlist.append(exception)
            except:
                exception = [pnumber,cnumber,category,"not available"]
                exlist.append(exception)
writer = csv.writer(ex)
writer.writerow(["PID","Claim","AMTCategory","ourCategory"])
writer.writerows(exlist)
print(count,total)
            
