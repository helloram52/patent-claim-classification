import zipfile, os, csv, re, shutil
from optparse import OptionParser
import nltk

#constants
SPEECH_CODE_VERB_GERUND = 'VBG'
STEP_NO_WORDS_TO_BE_CHECKED = 3 #no of words to be checked from beginning to conclude its a steps
STEP_EXCEPTION_WORDS = ['comprising', 'consisting', 'having'] #words which will be bypassed while searching for gerunds
#list of keywords to be identified as process claim - add some more if required
process_words = [["method", "process", "approach", "manner", "practice", "recipe", "scheme", "technique", "computer-implemented method", "treatment"]]
#list of keywords to be identified as product or productByProcess claim
product_words  = [["apparatus", "device", "machine", "machinery", "system", "product", "computer program product", "computer readable medium", "computer readable storage medium"]]
gerund = ""
######Tian isItem
def removeOrder(text):

    try:
        order = re.findall("\w{1,2}[\,\.]{1}",text[:3])[0]
        
        text=text[:3].replace(order,"")+text[3:]
    except:
        pass


    return text

def isItem(text):
    
    text = removeOrder(text)
    text = removeSpecialChars(text)
    token = nltk.word_tokenize(text)
    contextNoun=""
    
    if token == [] or token is None:
        return False


    try:
        if "means for" in text.split()[0:10] or ("means" in text.split()[0,7] and "said" in text.split()[0,7]):
            return True
    except:
        if "means for" in text.split() or ("means" in text.split() and "said" in text.split()):
            return True


    ###tag the first 10 words(or all if text shorter than 10 words) to figure out if there is noun or not.
    try:
        tagged = nltk.pos_tag(token[0:10])
    except:
        tagged = nltk.pos_tag(token)
 
    #print(tagged)
    nounFlag = False
    
    for i in range(0,11):
        try:
            ithtuple=tagged[i]
            #NN,NNS,etc are standing for Nouns
            if "NN" in ithtuple[1]:
                nounFlag = True
                contextNoun = ithtuple[0]
                break
        except:
            break
    #no Noun, not an item, false returned
    if nounFlag == False:
        return False
    else:
        #the position of the noun in the context
        position = token.index(contextNoun)

        ######retun false here
        #Freezing a banana
        if position==2 and tagged[position-2][1] =="VBG" and tagged[position-1][1] in ["CD","DT"]:
            return False
            
        elif position==3 and tagged[position-3][1] =="VBG" and tagged[position-2][1] in ["CD","DT"] and tagged[position-1][1] =="JJ":
            return False

        tagged.append(("end","end"))

        #Instances of sentences:
        #1. starting with noun, pos = 0
        #e.g. Book is a collection of paper. 
        if position == 0:
            return True
        #2. starting with a determiner(DT) like the, a , this , that, and then followed by the noun, pos = 1
        #e.g. The book is terrible
        elif position==1 and tagged[0][1]=="DT":
            return True
        #3. starting with a cardinal number like 3, 10, then followed by noun , pos =  1
        #e.g. 10 books cost me 10 bucks.
        elif position==1 and tagged[0][1]=="CD":
            return True
        elif position>=1 and tagged[position-1][1] == "VBG" and tagged[position][1]=="NNS" and tagged[position+1][1]=="VBP":
            return True
        elif position>=2 and tagged[position-2][1] in ["CD","DT"] and tagged[position-1][1] == "VBG" and tagged[position][1]=="NN":
            return True
        ##warning, this part needs update for gerund form checking,
        ##bug example: "burning this book is ...." is determined as an Item by 4 5 6 7
        # 4. no idea what the sentence starts with, but [determiner] + [noun] is contained
        # e.g. ... these books ...
        elif position>=2 and tagged[position-1][1]=="DT" and tagged[position-2][1] != "VBG":
            return True
        # 5. no idea what the sentence starts with, but [number] + [noun] is contained
        # e.g. ... 10 books ..
        elif position>=2 and tagged[position-1][1]=="CD" and tagged[position-2][1] != "VBG":
            return True
        # 6. no idea what the sentence starts with, but [number] +[adjective] + [noun] is contained
        # e.g. ... 10 good books ..
        elif position>=2 and tagged[position-1][1]=="JJ" and tagged[position-2][1]=="CD":
            return True
        # 7. no idea what the sentence starts with, but [number] +[adjective] + [noun] is contained
        # e.g. ... my good books ..
        elif position>=2 and tagged[position-1][1]=="JJ" and tagged[position-2][1]=="DT":
            return True
        # can add more here
        elif tagged[position+1][1]=="VBG" and True not in [x[1]=="VBG" for x in tagged[:position]]:
            return True
        elif True not in [x[1]=="VBG" for x in tagged]:
            return True
        else:
            return False

####Tian reprocess
def reprocess(csvfileadd):

    csvfile = csvfileadd
    #test
    #print(isItem("a first layer selected from the group consisting of silicon dioxide (SiO.sub.2) and a mixture of silicon dioxide (SiO.sub.2) and alumina (Al.sub.2 O.sub.3) deposited on said synthetic resin base by evaporation, said first layer having a geometrical film thickness of 1 to 5.mu.;"))

    #1st open to read number of lines
    file2 = open(csvfile,"r+")
    file1 = csv.reader(file2)
    length = len(list(enumerate(file1)))
    file2.close()


    #2nd open for processing
    file2 = open(csvfile,"r+")
    file1 = csv.reader(file2)

    #the line number of preamble
    locator = -1
    #patent number
    pnumber=-1
    #type mark
    mark="-1"
    #item check result for lv 2
    items2=[]
    #step check result for lv2
    step2=[]
    #for level 3
    items3=[]
    #for level 3
    step3=[]
    # instead of saying all level 2 are step, we might change the condition to 80% are step in the future. So lets save this for future use
    ratio = 1
    # list for rewriting
    parsed = []
    
    for i,eachline in enumerate(file1):
        text = removeOrder(eachline[2])
        
        text = removeSpecialChars(text)
        #append eachlin to parsed
        parsed.append(eachline)

        if parsed[i][6] in ["2","3",2,3]:
            if (isItem(text)):
                parsed[i][7]="2"
            elif (isStep(text)):            
                parsed[i][7]="1"
            else:
                parsed[i][7]="0"
        elif parsed[i][6] in [4,5,"4","5"]:
            try:
                parsed[i][7]="0"
            except IndexError:
                parsed[i].append("0")

        #start detrming every time the patent number changes
        '''
        if pnumber in [6065121,"6065121"]:
            print eachline[1]
        '''
        if pnumber != eachline[1] and pnumber != -1:
            #simply the rule
            if((True in step2) and (True not in step3)):
                mark = "1"
            elif(((items2.count(True)>items2.count(False)) and (True not in step3))):
                mark = "2"
            elif(((items2.count(True)>items2.count(False)) and (True in step3))):
                mark = "3"
            elif ((len(items2) +len(step2) +len(items3) +len(step3)) ==0):
                if isItem(text):
                    mark="2"
                elif isStep(text):
                    mark="1"
                else:
                    mark="0"
            else:
                mark = "0"
           
           
           
           
           
            #print(parsed[locator][7])
            #edit parsed.
                #print (mark,parsed[locator])
            try:
                parsed[locator][7]=mark
            except IndexError:
                parsed[locator].append(mark)
            
            locator = -1
            pnumber = -1
            mark = "-1"
            items2=[]
            step2=[]
            items3=[]
            step3=[]
            

        #redo type 0 and type 3-Feb(acatully 2/3 as you input in the previous code)
        try:
            if eachline[6]=="1" and eachline[7] not in ["1"]:
                
                locator = i
                
                pnumber = eachline[1]
        except IndexError:
            if eachline[6]=="1":
                
                locator = i
                
                pnumber = eachline[1]
        '''
        if pnumber in [6065121,"6065121"]:
            print(mark,"what what")
        '''
        if (eachline[1]==pnumber):
            if eachline[6] == "2":
                #determine
                items2.append(isItem(text))
                step2.append(isStep(text))
            if eachline[6] == "3":
                step3.append(isStep(text))
                items3.append(isItem(text))
        #end of file, the patent won't change for current patent, so start determing
        if (i == length-1):
            
            if((True in step2) and (True not in step3)):
                mark = "1"
            elif(((items2.count(True)>items2.count(False)) and (True not in step3))):
                mark = "2"
            elif(((items2.count(True)>items2.count(False)) and (True in step3))):
                mark = "3"
            elif ((len(items2) +len(step2) +len(items3) +len(step3)) ==0):
                if isItem(text):
                    mark="2"
                elif isStep(text):
                    mark="1"
                else:
                    mark="0"
            else:
                mark = "0"
                
            parsed[locator][7]=mark
            #print(items2,step3,locator,mark,i)
            locator = -1
            pnumber = -1
            mark = "-1"
            items2=[]
            step2=[]
            items3=[]
            step3=[]

    #overwrite the file by parsed list.
    with open(csvfile,"w+") as what:
        
        writer  = csv.writer(what,quoting=csv.QUOTE_ALL)
        writer.writerows(parsed)
        
    what.close()
   


####


#Process each csv file (output of splitter code) and identify claim type
def categorizer(files, outputFile):
	with open(outputFile, 'w') as outputCSV:
		outputCSV = csv.writer(outputCSV)
		for rawfile in files:
			with open(rawfile, 'rU') as rawTextFile:
				csvFile = csv.reader(rawTextFile)
				rownum = 1
				for line in csvFile:
					if rownum == 1:#add header fields here (if required)
						line.append("Type");
						line.append("is gerund?(Level 2 or above)");
						line.append("gerund value");
					else:
						#remove any special characters (other than space) for parsing accurately
                        
						text = removeSpecialChars(removeOrder(line[2]))
						if line[6] == "1": #preamble
							if line[4] == "0": #independent claim
								patentType = getPreambleClaimType(text)
								line.append(patentType)
								#meansPatent = checkMeansPatent(text)
								#line.append(meansPatent)
						elif line[6] in ("2","3","4"):
								stepFlag = isStep(text)
								itemFlag = isItem(text) 
								if stepFlag and not itemFlag:
									print "Message: step found: " + line[1] + "-" + line[3] + "-" + line[6]
									#line.append("1")
									line.append("Step")
									line.append(gerund)
								elif not stepFlag and itemFlag:
									print "Message: item found: " + line[1] + "-" + line[3] + "-" + line[6]
									#line.append("2")
									line.append("Item")
								elif stepFlag and itemFlag:
									line.append("Item & Step")
									line.append("STEP Data: Gerund=" +gerund)
								else:
									line.append("Don't know")
									line.append("Neither step nor item")

					rownum += 1
					outputCSV.writerow(line)

	reprocess(outputFile)

#check whether "means" keyword exists - return 1 if present, otherwise return 0
def checkMeansPatent(text):
	lowerCaseText = text.lower()
	pattern=re.compile(r'means')
	searchObj = pattern.search(lowerCaseText)
	if searchObj:
		return "1"
	else:
		return "0"

#check whether preamble is process or product claim - if both keywords are present or none of the keywords are present, return claim type as 0 (i.e., I don't know)
def getPreambleClaimType(text):
	processPatent = isProcessPatent(text)
	productPatent = isProductPatent(text)
	validType = True if(processPatent != productPatent) else False
	if validType:
		patentType = "1" if processPatent else "2/3"
	else:
		patentType = "0"

	return patentType

#check whether the text indicates its a step - for levels 2 or higher
def isStep(text):
        ######Added by Tian, assuming a text cannot be item and step at the same time
        if isItem(text):
            return False
        ###################
	wordList = text.split()
	taggedWords = getSpeechCodes(text) #get speech code for all words in the text
	speechCodeString = setupSpeechCodeString(taggedWords) #compose a new string with the speech code for each word
	speechCodeWords = speechCodeString.split()
	countOfWords = len(wordList) if len(wordList) < STEP_NO_WORDS_TO_BE_CHECKED else STEP_NO_WORDS_TO_BE_CHECKED
	for index in xrange( countOfWords ): #search for gerund
		code = speechCodeWords[index]
		if SPEECH_CODE_VERB_GERUND == code:
			bypassCheck = isWordPresent(wordList[index], STEP_EXCEPTION_WORDS) #check whether the word is present in exception list
			if not bypassCheck:
				print "found Gerund: in first "+ str(countOfWords) + " words: " + wordList[index]
				global gerund
				gerund = wordList[index]
				return True
	return False

#check whether given word (arg1) exists in the word list (arg2)
def isWordPresent(word, wordList):
	for index in xrange(len(wordList)):
		if word == wordList[index]:
			return True
	return False

#compose a new string with the speech code for each word
def setupSpeechCodeString(taggedWords):
	speechCodeString = ""
	for taggedWord in taggedWords: 
		speechCodeString += taggedWord[1] + " "
	#print speechCodeString
	return speechCodeString

#get speech code for each word eg., [('5', 'CD'), ('A', 'NNP'), ('method', 'NN'), ('comprising', 'VBG')]
def getSpeechCodes(text):
	wordList = text.split()
	taggedWords = nltk.pos_tag(wordList)
	return taggedWords

#remove special character (other than space) from given string and return result
def removeSpecialChars(text):
	pattern=re.compile("[^\w\s]")
	return pattern.sub('', text)

#check whether either one of the process keyword is present and return 
def isProcessPatent(preamble):
	words = ['|'.join(x) for x in process_words]
	expression = r'({})'.format(*words)
	pattern = re.compile(expression, re.X)
	searchObj = pattern.search(preamble)
	return True if searchObj else False

#check whether either one of the product keyword is present
def isProductPatent(preamble):
	words = ['|'.join(x) for x in product_words]
	expression = r'({})'.format(*words)
	pattern = re.compile(expression, re.X)
	searchObj = pattern.search(preamble)
	return True if searchObj else False


# Helper subroutine to parse options from the user
def parseUserOptions():
	parser = OptionParser()
	parser.add_option("-d", "--dir", dest = "zipfiledirectory", type = "string", help = "Include directory that contains the zip file(s). Defaults to the current directory.", metavar = "ZIP_FILE_DIRECTORY")
	parser.add_option("-z", "--zipfile", dest = "zipfile", help = "Parse only the given comma separated list of zip file(s). Each file should be fully qualified i.e. /Users/username/a.zip,/Users/username/b.zip", metavar = "ZIP_FILE_PATH")
	parser.add_option("-f", "--file", dest = "textfile", help = "Parse only the given comma separated list of txt file(s). Each file should be fully qualified.", metavar = "TXT_FILE_PATH")
	parser.add_option("-o", "--outputdir", dest = "outputdir", help = "Writes the output.csv + exception.csv files to the specified directory. Defaults to the current directory.", metavar = "ZIP_FILE_PATH")
	parser.add_option("-q", "--quiet", action = "store_false", dest = "verbose", default = True, help = "Avoids printing status messages to the terminal")

	(userOptions, args) = parser.parse_args()

	if userOptions.zipfiledirectory and (userOptions.zipfile or userOptions.textfile):
		parser.error("Please specify only one of -d (or) -f (or) -z.")
	elif not (userOptions.zipfiledirectory or userOptions.zipfile or userOptions.textfile):
		print "No options specified. Picking up zipfiles in the current directory for parsing"
		userOptions.zipfiledirectory = os.getcwd()

	tempDirectory = ''#create temp directory for files extracted from zipped files

	#store the input type
	inputType = 'z' if (userOptions.zipfiledirectory or userOptions.zipfile) else 'f'

	# List to store zip files
	zipfiles = []

	# If user has specified a directory that contains the zip files, get location of all the zip files.
	if userOptions.zipfiledirectory:
		for filename in os.listdir( userOptions.zipfiledirectory ):
			if filename.endswith(".zip"):
				zipfiles.append( os.path.join(userOptions.zipfiledirectory, filename) ) 

	# Otherwise, if a list of zip files are given, collect those for unzipping.
	elif userOptions.zipfile:
		zipfiles = userOptions.zipfile.split(',')

	txtfiles = []
	# If the user has explicitly given a list of comma separated txt files, get them.
	if userOptions.textfile:
		txtfiles = userOptions.textfile.split(',')
	# Else if there are zip files to extract, check if they exist, unzip them to the current folder(where the script is run)
	# and add them to our list of to-be-parsed text files.
	elif len(zipfiles) > 0:
		tempDirectory = createTempDirectory( os.getcwd() )
		for zippedFile in zipfiles:
			if os.path.exists(zippedFile):
				with zipfile.ZipFile(zippedFile, 'r') as theZip:
					theZip.extractall(tempDirectory)
		for inputFile in os.listdir(tempDirectory):
		    if inputFile not in ('.' , '..'):
	    		txtfilename = tempDirectory + inputFile
	    		txtfiles.append(txtfilename)

	# Alert the user if there no files to parse
	if len(txtfiles) == 0:
		print "No files to parse. Exiting.."
		return

	outputdir = userOptions.outputdir or os.getcwd()
	outputFile = outputdir
	# Create the output dir if one doesn't exist already
	if not os.path.exists(outputdir):
		os.makedirs(outputdir)

	if outputdir[-1] != '/':
		outputFile += '/'

	exceptFile = outputFile + 'exceptions.csv'
	outputFile += 'output.csv'

	return {
		'textfiles' : txtfiles, #input text files
		'outputfile' : outputFile,#outputfile name
		'exceptfile' : exceptFile, #exception file name
		'inputType' : inputType,#required later to remove temporary files created by unzipping input files.
		'tempDirectory' : tempDirectory #temporary directory where files created by unzipping input files is stored
	}

#delete the temporary files and temporary directory
def deleteTempFiles(tempDirectory):
	shutil.rmtree(tempDirectory)

#create temp directory for files extracted from zipped files
def createTempDirectory(currentDirectory):
	directory = currentDirectory+'/.temp/'
	if not os.path.exists( directory ):
		os.makedirs( directory )

	return directory

#main function
if __name__ == '__main__':
	parsedOptions = parseUserOptions()
	categorizer(parsedOptions['textfiles'], 'output_categorized.csv')
	if parsedOptions['inputType'] == 'z':
		deleteTempFiles( parsedOptions['tempDirectory'] )