import zipfile, os, csv, re, shutil
from optparse import OptionParser

claimWords = ["claiom","recited"] # words to be replaced as claim temporarily for processing
claimWordsToBeExcluded=['claimant'] #words to be excluded i.e. not to be considered as claim words
knownTyposOfClaim = ("ofclaim", "theclaim", "toclaim") # words to look for if 'claim' is not a prefix


def writeHeader(outputFile):
	with open(outputFile, 'a') as outputcsv:
		outputcsv = csv.writer(outputcsv)
		outputcsv.writerow(["id", "patent_id", "text", "claim", "dependency", "sequence", "level", "table/equation Marker"])

#write error message to log file
def writeLog(message):
	with open("log.txt", 'a') as logFile:
		logFile.write(message + "\n")

#check presence of table or Equation in text
def checkEquationTable(text):
	marker = "No"
	itemsList =["EQU", "TBL", "TABL3"]
	for item in itemsList:
		if item in text:
			marker="Yes"
			break
			 
	return marker
#remove semi-colons
def removeSemicolon(text):
	return re.sub(';',' ',text)

#check whether dependency should be 0, by checking if its claim 1 and level 1
def isDependencyOverRideRequired( seq ):
	return True if seq == 1 else False;


#write output records
def writeline(outputFile, patentnum, text, number, dep, seq, level):
	global cnum
	marker = checkEquationTable(text)
	text = removeSemicolon(text)
	if ( isDependencyOverRideRequired( seq ) ):
		dep = 0
	with open(outputFile, 'a') as outputcsv:
		outputcsv = csv.writer(outputcsv)
		outputcsv.writerow([cnum, patentnum, text, number, dep, seq, level, marker])
	cnum += 1

#write name of the file for ease of locating the source
def writeFileName(exceptFile, fileName):
	with open(exceptFile, 'a') as exceptfileCSV:
		exceptfileCSV = csv.writer(exceptfileCSV)
		exceptfileCSV.writerow([fileName])

def findClaimNumber(string):
	pattern=re.compile("[^\w]")
	string=pattern.sub(' ', string)
	num_found=False #indicates whether a number is found in the string
	num=""
	minVal=99999
	for char in string:#iterate each character of the word next to "claim"
		if char.isdigit():
			num_found=True
			num+=str(char)
		else:
			if num_found:#break the loop if claim number is already found
				num_found=False
				minVal=min(minVal, int(num))
				num=""
	#if it has only one character, return 1
	#if num_found == False and len(string) == 1:
	#	return -1
	if num != "":
		minVal=min(minVal, int(num))

	return str(minVal) if minVal != 99999 else ""

#Get start position of the word which contains the given index in the line
def getStartPositionOfWord(line, claimPos):
	index = claimPos
	while line[index] != ' ':
		index -= 1
	return index

#find and replace claimWords in the given line
def findAndReplaceClaimTypoWords( line ):
	for word in claimWords:
		if line.rfind( word ) > -1:
			line = line.replace( word, "claim" )
	return line 

#Find Last occurrence of the "preceding claim" keyword where it is
#	1. a prefix
#	2. singular
#	3. doesnt have 'any' keyword in previous 3 words
# returns 2 items
#	1. flag - True if preceding claim keyword is found, false otherwise
#	2. ind - 0 if not found (future use), 1 if preceding claim refers the immediate previous claim, 2 if preceding claim is in plural form or 'any of the preceding claim'
def isPrecedingClaimKeywordExist( line ):
	claimKeyWordFound = False
	keyWord = "preceding claim"
	claimLasPos = line.rfind(keyWord) #find last occurrence of claim again
	precedingClaimInd = 0
	if claimLasPos > -1:
		claimKeyWordFound = True
		if line[claimLasPos-1] == ' ' and line[claimLasPos + len(keyWord)] == ' ':#if preceding claim is a prefix of a word and singular
			splitWords = line.split()
			fromIndex = splitWords.index( "preceding" ) - 3 #get index of word, that is 3 words before "preceding" keyword
			anyKeywordFound = False
			for index in xrange(3):
				if( splitWords[fromIndex + index] == 'any' ):
					anyKeywordFound = True
					break
			if not anyKeywordFound:
				precedingClaimInd = 1
			else:
				precedingClaimInd = 2
		elif line[claimLasPos-1] == ' ':
				precedingClaimInd = 2

	claimKeyWordFound = True if claimLasPos != -1 else False # set the flag
	return claimKeyWordFound, precedingClaimInd

#Get dependency of a given claim by parsing the text. Applies the rule below.
#Find Last occurrence of the "claim" keyword where it is
#	1. a prefix
#	2. part of a word (due to typos) i.e. of-claim will be treated as claim but reclaim will not be considered as claim.
def isClaimKeywordExist(line):
	claimKeyWordFound = False
	#claimLasPos = line.rfind("claim") #find last occurrence of claim
	#line = findAndReplaceClaimTypoWords( line ) if claimLasPos == -1 else line # find and replace claim typos with 'claim' keyword
	line = findAndReplaceClaimTypoWords( line ) 
	claimLasPos = line.rfind("claim") #find last occurrence of claim again
	while claimLasPos > -1:
		startPos = getStartPositionOfWord(line, claimLasPos)
		claimWord = line[startPos:].split()[0]
		if line[claimLasPos-1] == ' ' and claimWord not in claimWordsToBeExcluded:#if claim is a prefix of a word
			claimKeyWordFound = True
			break
		else:
			#startPos = getStartPositionOfWord(line, claimLasPos)
			#claimWord = line[startPos:].split()[0]
			pattern=re.compile(".*[^\w]claim")
			if pattern.match(claimWord) is not None:
				claimKeyWordFound = True
				break
			elif claimWord in knownTyposOfClaim:
				claimKeyWordFound = True
				break
		claimLasPos = line[0:claimLasPos].rfind("claim") #find last occurrence of claim	

	claimKeyWordFound = True if claimLasPos != -1 else False # set the flag
	return claimKeyWordFound, claimLasPos

#Get dependency of a given claim by parsing the text. Applies the rule below.
#Check whether claim keyword exists
#if claim word is found in string:
#	Split the string into words, starting from the word where the "claim" keyword was found, look for number in the next 5 words (inclusive of claim word)
#	if claim number is found, 
#		check whether it is greater than current claim number, 
#			if it is then reduce it to 1
#			else set it as dependency
#	if claim number not found, 
#		set 1 as dependency and write error message into log
#if claim word is not found in string:
#	set 0 as dependency and write message into log
def get_dep(line, number, patent, map):
	depResolved = False #flag to indicate dependency resolved or not
	noWordsToCheck = 5 #no of words to check for claim number (inclusive of claim word)

	#check if 'preceding claim' keyword is found
	precedingClaimKeyWordFound, precedingClaimInd = isPrecedingClaimKeywordExist( line.lower() )
	dep = 0
	if precedingClaimKeyWordFound: #if found
		if precedingClaimInd == 1: #if preceding claim is singular and refers previous claim, assign claim t-1 as dependency
			if number > 1:
				dep = map[number-1]
			elif number == 1:
				dep = map[number]
		elif precedingClaimInd == 2:#if preceding claim is in plural form, assign claim 1 as dependency
			dep = 1
		return dep

	#check if claim keyword (looks for typos as well) is found
	claimKeyWordFound, claimLasPos = isClaimKeywordExist( line.lower() )

	if claimKeyWordFound:
		while depResolved == False and claimKeyWordFound == True and claimLasPos != -1:
			wordList = line[claimLasPos:].split() #split the string from that position
			dep = 1
			minClaimNumber = 99999
			for index in xrange(noWordsToCheck):#look for claim number in next 5 words (including claim word), will stop once found
				try:
					depClaim = findClaimNumber(wordList[index])
					try1 = int(depClaim)
					if try1 > number:
						try1 %= 10
					try1 = 1 if try1 == 0 or try1 > number else try1
					if try1 < minClaimNumber:
						minClaimNumber = try1
				except (ValueError, KeyError, IndexError) as e:
					continue
			try:
				if number > 1 and number == minClaimNumber: #if claim refers itself assign dependency as 1
					dep = map[1]
				else:
					dep = map[minClaimNumber]
					#print "final min claim number="+ str(minClaimNumber)
				depResolved = True
			except(ValueError, KeyError, IndexError) as e:
				depResolved = False
			#attempt to see whether another claim keyword is present
			claimKeyWordFound, claimLasPos = isClaimKeywordExist( line.lower()[:claimLasPos] )

		if not depResolved:
			message = "Error: "+str(patent)+"- claim reference found but couldn't resolve dependency for claim " + str(number)+ ". Assigning 1 as dependency."
			writeLog(message)

	else:
		message = "Message: "+str(patent)+"- claim reference not found for " + str(number)+ ". Assigning 0 as dependency."
		writeLog(message)
		dep = 0
	return dep

cnum = 0
def parsePatentInfo(files, outputFile, exceptFile):
	for rawfile in files:
		with open(rawfile, 'r') as rawtextfile:
			"""
			the structure of this file doesn't really lend itself well to a better approach
			so we have to iterate through and use a lot of storage variables to keep things going
			"""
			claims_found = False
			number = 0 # claim number
			seq = 0 # sequence number
			patent = 0 # patent number
			sub = 0 # subclaim dependent on
			sub2 = 0 # claim a second-level is dep on
			sub3 = 0 # so on
			sub4 = 0
			done = True # done with a line
			ltext = "" # text of a line
			flag = "" # flag for which level we're on
			level = 0 #indicates which PAR level we're on [PAR-1, PA1-2, PA2-3...] 
			map = {} # map for claims to sequence numbers
			previousNUM=0 # stores previous claim number
			ExceptFlag=False
			writeFileName(exceptFile, rawfile)
			writeHeader(exceptFile)
			writeHeader(outputFile)
			writeLog("File Name:" + rawfile) #write file name in log file
			for line in rawtextfile: # iterate
				#if "PATN" in line: # check the start of a patent
				if line[0:4] == "PATN": # check the start of a patent
					if flag == "PAR": # anything going on? print it to the file
						#if "claim" in ltext: # check for dependency in the text of the claim
						dep = get_dep(ltext, number, patent, map)
						if not ExceptFlag:
							writeline(outputFile, patent, ltext, number, dep, seq, level)
							ltext = ""
						else:
							writeline(exceptFile, patent, ltext,number, dep, seq, level)
							ExceptFlag=False
							ltext=""
					if flag == "PA1":
						if not ExceptFlag:
							#writeline(outputFile, patent, ltext, number, sub, seq, level)
							writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
							ltext = ""
						else:
							writeline(exceptFile, patent, ltext,number, dep, seq, level)
							ExceptFlag=False
							ltext=""
					if flag == "PA2":
						if not ExceptFlag:
							#writeline(outputFile, patent, ltext, number, sub2, seq, level)
							writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
							ltext = ""
						else:
							writeline(exceptFile, patent, ltext,number, dep, seq, level)
							ExceptFlag=False
							ltext=""
					if flag == "PA3":
						if not ExceptFlag:
							#writeline(outputFile, patent, ltext, number, sub3, seq, level)
							writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
							ltext = ""
						else:
							writeline(exceptFile, patent, ltext,number, dep, seq, level)
							ExceptFlag=False
							ltext=""
					if flag == "PA4":
						if not ExceptFlag:
							#writeline(outputFile, patent, ltext, number, sub4, seq, level)
							writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
							ltext = ""
						else:
							writeline(exceptFile, patent, ltext,number, dep, seq, level)
							ExceptFlag=False
							ltext=""
					flag = "" # reset everything that's patent-specific
					claims_found = False
					seq = 0
					number = 0
					patent = 0
					sub = 0
					sub2 = 1
					sub3 = 1
					sub4 = 1
					level = 0
					done = True
					map = {}
					previousNUM=0
				#elif "CLMS" in line: # if claims are found, begin looking at stuff
				elif line[0:4] == "CLMS": # if claims are found, begin looking at stuff
					claims_found = True

				# Patent Number is of the form 'WKU 9_DIGIT'
				# of which last digit(9th) is a checksum that has to be skipped.
				#if "WKU" in line:
				if line[0:3] == "WKU":
					patent = line.split()[1][:-1]
				if claims_found: # if we're looking
					#if "PAR" in line: # if it's a paragraph
					if line[0:3] == "PAR": # if it's a paragraph
						#print "previousNUM="+str(previousNUM)+" Num="+str(number)
						if previousNUM != number:
							dep = 0 # assume it's an independent claim
							seq += 1 # up the sequence number
							sub = seq # store that number in case a dependent claim needs it
							ltext = ltext + " " + line[4:].lstrip().rstrip() # add text to variable
							flag = "PAR" # flag that we're doing a paragraph for future lines
							done = False # assume there are more lines
							previousNUM=number
							level = 1
							ExceptFlag=False
							prevSeq = seq
							continue # skip everything below
						else:
							#print "Excep: "+patent+"- PAR appeared without NUM. Refer Exception file."
							#Write out the previous record
							if flag!= "":
								if flag == "PAR":
									#if "claim" in ltext: # check for dependency in the text of the claim
									dep = get_dep(ltext, number, patent, map)
									if not ExceptFlag:
										writeline(outputFile, patent, ltext, number, dep, seq, level)
										ltext = ""
									else:
										writeline(exceptFile, patent, ltext, number, dep, seq, level)
										ltext = ""
								if flag == "PA1":
									if not ExceptFlag:
										writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
										ltext = ""
									else:
										writeline(exceptFile, patent, ltext, number, dep, seq, level)
										ltext = ""
								if flag == "PA2":
									if not ExceptFlag:
										writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
										ltext = ""
									else:
										writeline(exceptFile, patent, ltext, number, dep, seq, level)
										ltext = ""
								if flag == "PA3":
									if not ExceptFlag:
										writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
										ltext = ""
									else:
										writeline(exceptFile, patent, ltext, number, dep, seq, level)
										ltext = ""
								if flag == "PA4":
									if not ExceptFlag:
										writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
										ltext = ""
									else:
										writeline(exceptFile, patent, ltext, number, dep, seq, level)
										ltext = ""
							dep = 0 # assume it's an independent claim
							seq += 1 # up the sequence number
							prevSeq = seq
							#sub = seq # store that number in case a dependent claim needs it
							ltext = ltext + " " + line[4:].lstrip().rstrip() # add text to variable
							flag = "PAR" # flag that we're doing a paragraph for future lines
							ExceptFlag=True
							level = 1
							done=False
							continue
					#if "NUM" in line: # if a claim number is encountered
					if line[0:3] == "NUM": # if a claim number is encountered
						if not done: # print everything out, because this marks the end of what we were on before
							done = True
							if flag == "PAR":
								#if "claim" in ltext: # check for dependency in the text of the claim
								dep = get_dep(ltext, number, patent, map)
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, dep, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA1":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA2":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA3":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA4":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							flag = ""
						try: # try to grab that number
							number = int(''.join(c for c in line if c.isdigit()))
							map[number] = seq + 1 # map in the claim number with the sequence number we're about to use
						except ValueError:
							number = 0
						continue # skip to the next line
					#if "PA" in line: # if a subclaim is here
					if line[0:2] == "PA": # if a subclaim is here
						if "PAL" in line:
							ltext = ltext + " " + line[4:].lstrip().rstrip()
							continue
						if not done: # print out if we're not done with the previous
							done = True
							if flag == "PAR":
								#if "claim" in ltext: # check for dependency in the text of the claim
								dep = get_dep(ltext, number, patent, map)
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, dep, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA1":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA2":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA3":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							if flag == "PA4":
								if not ExceptFlag:
									writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
									ltext = ""
								else:
									writeline(exceptFile, patent, ltext, number, dep, seq, level)
									ltext = ""
							flag = ""
						prevSeq = seq
						seq += 1 # increment seq
						#if "PA1" in line: # do stuff here to store the sequence numbers for any subs (only 4 levels max)
						if line[0:3] == "PA1": # do stuff here to store the sequence numbers for any subs (only 4 levels max)
							flag = "PA1"
							level = 2
							ltext = ltext + " " + line[4:].lstrip().rstrip()
							#sub2 = seq
						#if "PA2" in line:
						if line[0:3] == "PA2":	
							flag = "PA2"
							level = 3
							ltext = ltext + " " + line[4:].lstrip().rstrip()
							#sub3 = seq
						#if "PA3" in line:
						if line[0:3] == "PA3":
							flag = "PA3"
							level = 4
							ltext = ltext + " " + line[4:].lstrip().rstrip()
							#sub4 = seq
						#if "PA4" in line:
						if line[0:3] == "PA4":
							flag = "PA4"
							level = 5
							ltext = ltext + " " + line[4:].lstrip().rstrip()
						done = False
						continue
					if not done:
						ltext = ltext + " " + line[4:].lstrip().rstrip()

			#write the last record
			if flag == "PAR": 
				#if "claim" in ltext: 
				dep = get_dep(ltext, number, patent, map)
				if not ExceptFlag:
					writeline(outputFile, patent, ltext, number, dep, seq, level)
					ltext = ""
				else:
					writeline(exceptFile, patent, ltext, number, dep, seq, level)
					ltext = ""
			elif flag == "PA1":
				if not ExceptFlag:
					writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
					ltext = ""
				else:
					writeline(exceptFile, patent, ltext, number, dep, seq, level)
					ltext = ""
			elif flag == "PA2":
				if not ExceptFlag:
					writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
					ltext = ""
				else:
					writeline(exceptFile, patent, ltext, number, dep, seq, level)
					ltext = ""
			elif flag == "PA3":
				if not ExceptFlag:
					writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
					ltext = ""
				else:
					writeline(exceptFile, patent, ltext, number, dep, seq, level)
					ltext = ""
			elif flag == "PA4":
				if not ExceptFlag:
					writeline(outputFile, patent, ltext, number, prevSeq, seq, level)
					ltext = ""
				else:
					writeline(exceptFile, patent, ltext, number, dep, seq, level)
					ltext = ""


#create temp directory for files extracted from zipped files
def createTempDirectory(currentDirectory):
	directory = currentDirectory+'/.temp/'
	if not os.path.exists( directory ):
		os.makedirs( directory )

	return directory

# Helper subroutine to parse options from the user
def parseUserOptions():
	parser = OptionParser()
	parser.add_option("-d", "--dir", dest = "zipfiledirectory", type = "string", help = "Include directory that contains the zip file(s). Defaults to the current directory.", metavar = "ZIP_FILE_DIRECTORY")
	parser.add_option("-z", "--zipfile", dest = "zipfile", help = "Parse only the given comma separated list of zip file(s). Each file should be fully qualified i.e. /Users/username/a.zip,/Users/username/b.zip", metavar = "ZIP_FILE_PATH")
	parser.add_option("-f", "--file", dest = "textfile", help = "Parse only the given comma separated list of txt file(s). Each file should be fully qualified.", metavar = "TXT_FILE_PATH")
	parser.add_option("-o", "--outputdir", dest = "outputdir", help = "Writes the output.csv + exception.csv files to the specified directory. Defaults to the current directory.", metavar = "ZIP_FILE_PATH")
	parser.add_option("-q", "--quiet", action = "store_false", dest = "verbose", default = True, help = "Avoids printing status messages to the terminal")

	(userOptions, args) = parser.parse_args()

	if userOptions.zipfiledirectory and (userOptions.zipfile or userOptions.textfile):
		parser.error("Please specify only one of -d (or) -f (or) -z.")
	elif not (userOptions.zipfiledirectory or userOptions.zipfile or userOptions.textfile):
		print "No options specified. Picking up zipfiles in the current directory for parsing"
		userOptions.zipfiledirectory = os.getcwd()

	tempDirectory = ''
	#store the input type
	inputType = 'z' if (userOptions.zipfiledirectory or userOptions.zipfile) else 'f'

	# List to store zip files
	zipfiles = []

	# If user has specified a directory that contains the zip files, get location of all the zip files.
	if userOptions.zipfiledirectory:
		for filename in os.listdir( userOptions.zipfiledirectory ):
			if filename.endswith(".zip"):
				zipfiles.append( os.path.join(userOptions.zipfiledirectory, filename) ) 

	# Otherwise, if a list of zip files are given, collect those for unzipping.
	elif userOptions.zipfile:
		zipfiles = userOptions.zipfile.split(',')

	txtfiles = []
	# If the user has explicitly given a list of comma separated txt files, get them.
	if userOptions.textfile:
		txtfiles = userOptions.textfile.split(',')

	# Else if there are zip files to extract, check if they exist, unzip them to the current folder(where the script is run)
	# and add them to our list of to-be-parsed text files.
	elif len(zipfiles) > 0:
		tempDirectory = createTempDirectory( os.getcwd() )
		for zippedFile in zipfiles:
			if os.path.exists(zippedFile):
				with zipfile.ZipFile(zippedFile, 'r') as theZip:
					theZip.extractall(tempDirectory)
		for inputFile in os.listdir(tempDirectory):
		    if inputFile not in ('.' , '..'):
	    		txtfilename = tempDirectory + inputFile
	    		txtfiles.append(txtfilename)

	# Alert the user if there no files to parse
	if len(txtfiles) == 0:
		print "No files to parse. Exiting.."
		return

	outputdir = userOptions.outputdir or os.getcwd()
	outputFile = outputdir
	# Create the output dir if one doesn't exist already
	if not os.path.exists(outputdir):
		os.makedirs(outputdir)

	if outputdir[-1] != '/':
		outputFile += '/'

	exceptFile = outputFile + 'exceptions.csv'
	outputFile += 'output.csv'

	return {
		'textfiles' : txtfiles, #input text files
		'outputfile' : outputFile,#outputfile name
		'exceptfile' : exceptFile, #exception file name
		'inputType' : inputType,#required later to remove temporary files created by unzipping input files.
		'tempDirectory' : tempDirectory #temporary directory where files created by unzipping input files is stored
	}

#delete the temporary files and temporary directory
def deleteTempFiles(tempDirectory):
	shutil.rmtree(tempDirectory)

parsedOptions = parseUserOptions()
if parsedOptions:
	parsePatentInfo(parsedOptions['textfiles'], parsedOptions['outputfile'], parsedOptions['exceptfile'])
	if parsedOptions['inputType'] == 'z':
		deleteTempFiles( parsedOptions['tempDirectory'] ) 